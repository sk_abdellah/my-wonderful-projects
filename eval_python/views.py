from django.http import HttpResponse
from django.shortcuts import render 
from django.db import models
from projets.models import Projet

def home(request):
    projets = Projet.objects.all()
    return render(request, 'home.html', {'projets':projets})

def contact(request):
    return render(request, 'contact.html')
