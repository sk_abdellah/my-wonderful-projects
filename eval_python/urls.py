from django.contrib import admin
from django.conf.urls import url, include
from . import views
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', views.home),
    url(r'^contact/', views.contact),
    
    url(r'^articles/', include('articles.urls')),

    url(r'^projets/', include('projets.urls')),
]

urlpatterns += staticfiles_urlpatterns()