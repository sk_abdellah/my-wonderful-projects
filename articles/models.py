from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Article(models.Model):
    title = models.CharField(max_length=255)
    slug = models.SlugField(blank=False)
    description = models.TextField()
    image_url = models.CharField(max_length=255)
    date = models.DateField(auto_now_add=True)
    author = models.ForeignKey(User, on_delete=models.CASCADE, default=None)

    def __str__(self):
        return self.title

    def snippet(self):
        return self.description[:50] + '...'

# python3 manage.py makemigrations
# python3 manage.py migrate