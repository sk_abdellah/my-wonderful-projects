from django.db import models

# Create your models here.
class Projet(models.Model):
    name = models.CharField(max_length=255)
    slug = models.SlugField(blank=False)
    description = models.TextField()
    image_url = models.CharField(max_length=255)
    technology = models.CharField(max_length=255)
    repo_url = models.CharField(max_length=255, blank=False)
    website_url = models.CharField(max_length=255, blank=False)
    date = models.DateField(auto_now_add=True)

    def __str__(self):
        return self.name

    def snippet(self):
        return self.description[:50] + '...'

# python3 manage.py makemigrations
# python3 manage.py migrate