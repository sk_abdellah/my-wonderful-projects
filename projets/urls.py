from django.conf.urls import url
from . import views

app_name = 'projets'

urlpatterns = [
    url(r'^$', views.projet_list, name='list'),
    url(r'^create/$', views.projet_create, name="create"),
    url(r'^(?P<slug>[\w-]+)/$', views.projet_detail, name="detail"),
    
    url(r'^edit/(?P<slug>[\w-]+)/$', views.projet_edit, name="edit"),
    url(r'^edit/$', views.projet_edit, name="edit"),

    url(r'^delete/(?P<slug>[\w-]+)/$', views.projet_delete, name="delete"),
]
