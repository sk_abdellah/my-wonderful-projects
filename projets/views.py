from django.shortcuts import render, redirect
from .models import Projet
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from . import forms
from django.shortcuts import get_object_or_404 

# Create your views here.
def projet_list(request):
    projets = Projet.objects.all()
    return render(request, 'projets/projet_list.html', {'projets':projets})

def projet_detail(request, slug):
    projet = Projet.objects.get(slug=slug)
    return render(request, 'projets/projet_detail.html', {'projet':projet})

@login_required
def projet_create(request):
    if request.method =='POST':
        form = forms.CreateProjet(request.POST)
        if form.is_valid():
            form.save()
            return redirect('projets:list')
    else:
        form = forms.CreateProjet()
    return render(request, 'projets/projet_create.html', {'form':form})

@login_required
def projet_edit(request, slug):
    context = {} 
    obj = get_object_or_404(Projet, slug = slug) 
    form = forms.CreateProjet(request.POST or None, instance = obj) 
    if form.is_valid(): 
        form.save() 
        return redirect('/projets/') 
    context["form"] = form 
    return render(request, 'projets/projet_edit.html', context)

@login_required
def projet_delete(request, slug):
    projet = Projet.objects.get(slug=slug)
    projet.delete()
    return redirect('/projets/')



