from django import forms
from . import models

class CreateProjet(forms.ModelForm):
    class Meta:
        model = models.Projet
        fields = ['name', 'slug', 'description', 'image_url', 'technology', 'repo_url', 'website_url']